// Algo_Tabou.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
#include <string>
#include <list>
#include <fstream>
#include <ctime>
#include <tuple>

using namespace std;

const int MAX_IT = 100;

struct Block {
	int hauteur = 0, largeur = 0, profondeur = 0, aire = 0;
};

struct BlockTabou {
	Block block;
	int time = 0;
};

int numberOfIteration() {
	return (7 + rand() % (10 + 1 - 7));
}

void findBestCandidate(int* hauteurS, list<Block>* C, list<Block>* S, list<Block>::iterator* itBlocToAdd) {
	*hauteurS = 0;
	for (list<Block>::iterator itC = C->begin(); itC != C->end(); itC++) {
		int hauteurTemp = itC->hauteur;
		bool foundPlaceTemp = false;
		for (list<Block>::iterator itS = S->begin(); itS != S->end(); itS++) {
			if (!foundPlaceTemp && itS->largeur > itC->largeur && itS->profondeur > itC->profondeur) {
				hauteurTemp += itS->hauteur;
				foundPlaceTemp = true;
			}
			else if (foundPlaceTemp && itS->largeur < itC->largeur && itS->profondeur < itC->profondeur) {
				hauteurTemp += itS->hauteur;
			}
		}
		if (hauteurTemp > * hauteurS) {
			*hauteurS = hauteurTemp;
			*itBlocToAdd = itC;
		}
	}
}

void updateT(list<BlockTabou>* T, list<Block>* C) {
	// Baisse les itérations dans T et retourne ceux qui sont terminés dans C
	for (list<BlockTabou>::iterator it = T->begin(); it != T->end(); it++) {
		it->time--;
		if (it->time == 0) {
			C->push_back(it->block);
			it = T->erase(it);
		}
	}
}

void updateS(list<Block>* S, list<Block>::iterator itBlocToAdd, list<BlockTabou>* T) {
	// premiere fois: la solution S est vide, on ajoute donc le bloc avec la hauteur maximum
	if (S->size() == 0) {
		S->push_back(*itBlocToAdd);
	}
	// S n'est pas vide
	else {
		bool blockIsAdded = false;
		for (list<Block>::iterator itS = S->begin(); itS != S->end(); itS++) {
			if (!blockIsAdded) {
				if (!(itS->largeur > itBlocToAdd->largeur && itS->profondeur > itBlocToAdd->profondeur)) {
					S->insert(itS, *itBlocToAdd);
					blockIsAdded = true;
				}
			}
			if (blockIsAdded) {
				if (!(itS->largeur < itBlocToAdd->largeur && itS->profondeur < itBlocToAdd->profondeur)) {
					BlockTabou newBlockTabou;
					newBlockTabou.block = *itS;
					newBlockTabou.time = numberOfIteration();
					T->push_back(newBlockTabou);
					itS = S->erase(itS);
					itS--;
				}

			}
		}
		if (!blockIsAdded) {
			S->push_back(*itBlocToAdd);
		}
	}
}

void updateSEtoile(int hauteurS, int* hauteurSEtoile, list<Block>* S_etoile, list<Block> S, int* iterationCourante) {
	//comparaison entre le nouveau S et S*
	if (hauteurS > * hauteurSEtoile) {
		S_etoile->clear();
		*S_etoile = S;
		*hauteurSEtoile = hauteurS;
		*iterationCourante = 0;
	}
	else {
		*iterationCourante += 1;
	}
}

void printInformation(bool afficherTour, bool afficherHauteur, list<Block> S_etoile, int hauteurSEtoile) {
	// print la tour
	if (afficherTour) {
		for (list<Block>::iterator it = S_etoile.begin(); it != S_etoile.end(); it++) {
			cout << it->hauteur << " " << it->largeur << " " << it->profondeur << endl;
		}
	}
	if (afficherHauteur) {
		cout << hauteurSEtoile << endl;
	}
}

void tabou(list<Block> C, bool afficherTour, bool afficherHauteur) {
	list<Block> S;
	list<BlockTabou> T;
	list<Block> S_etoile;
	int hauteurS = 0;
	int hauteurSEtoile = 0;
	int iterationCourante = 0;
	list<Block>::iterator itBlocToAdd;

	while (iterationCourante < MAX_IT) {
		findBestCandidate(&hauteurS, &C, &S, &itBlocToAdd);

		updateT(&T, &C);

		updateS(&S, itBlocToAdd, &T);

		C.erase(itBlocToAdd);

		updateSEtoile(hauteurS, &hauteurSEtoile, &S_etoile, S, &iterationCourante);

	}
	printInformation(afficherTour, afficherHauteur, S_etoile, hauteurSEtoile);
}


int main(int argc, char* argv[])
{
	list<Block> blockList;
	bool afficherTour = false;
	bool afficherTemps = false;
	bool afficherHauteur = false;

	//lecture fichier et creation des blocs
	string line;
	ifstream MyReadFile(argv[1]);

	while (getline(MyReadFile, line)) {
		Block newblock;
		string value = "";
		int position = 0;
		for (auto letter : line) {
			if (letter == ' ') {
				if (position == 0) {
					newblock.hauteur = atoi(value.c_str());
				}
				else if (position == 1) {
					newblock.largeur = atoi(value.c_str());
				}
				position++;
				value = "";
			}
			else {
				value = value + letter;
			}
		}
		newblock.profondeur = atoi(value.c_str());
		newblock.aire = newblock.largeur * newblock.profondeur;
		blockList.push_back(newblock);
	}
	for (int i = 0; i < argc; i++)
	{
		string argument = argv[i];
		if (argument == "-p") {
			afficherTour = true;
		}
		else if (argument == "-r") {
			afficherHauteur = true;
		}
		else if (argument == "-t") {
			afficherTemps = true;
		}
	}
	if (!blockList.empty())
	{
		clock_t begin = clock();

		tabou(blockList, afficherTour, afficherHauteur);

		clock_t end = clock();

		if (afficherTemps) {
			double elapsed_m_secs = (double(end - begin) / CLOCKS_PER_SEC) * 1000;
			cout << elapsed_m_secs << endl;
		}
	}
	else {
		cout << "Aucun bloc n'a été créé" << endl;
	}
}

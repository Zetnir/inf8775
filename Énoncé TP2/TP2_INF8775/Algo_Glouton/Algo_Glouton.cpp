// Algo_Glouton.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include <ctime>


using namespace std;


struct Block {
	int hauteur = 0, largeur = 0, profondeur = 0, aire = 0;
};

bool comparerAire(const Block& a, const Block& b)
{
	return a.aire > b.aire;
}


void glouton(vector<Block> blockList, bool afficherTour, bool afficherHauteur) {
	vector<Block> tour;
	int hauteurTour = 0;
	// ajoute la base de la tour, 
	tour.push_back(blockList.front());

	if (afficherTour) {
		cout << tour[0].hauteur << " " << tour[0].largeur << " " << tour[0].profondeur << endl;
	}
	hauteurTour = tour[0].hauteur;
	blockList.erase(blockList.begin());

	while (!blockList.empty()) {
		Block firstBlock = blockList.front();
		if (firstBlock.largeur < tour[tour.size() - 1].largeur && firstBlock.profondeur < tour[tour.size() - 1].profondeur) {
			tour.push_back(firstBlock);
			if (afficherTour) {
				cout << firstBlock.hauteur << " " << firstBlock.largeur << " " << firstBlock.profondeur << endl;
			}
			hauteurTour += firstBlock.hauteur;
		}
		blockList.erase(blockList.begin());
	}
	if (afficherHauteur) {
		cout << hauteurTour << endl;
	}
}

int main(int argc, char* argv[])
{
	vector<Block> blockList;
	int hauteurTour = 0;
	bool afficherTour = false;
	bool afficherTemps = false;
	bool afficherHauteur = false;

	//lecture fichier et creation des blocs
	string line;
	ifstream MyReadFile(argv[1]);
	while (getline(MyReadFile, line)) {
		Block newblock;
		string value = "";
		int position = 0;
		for (auto letter : line) {
			if (letter == ' ') {
				if (position == 0) {
					newblock.hauteur = atoi(value.c_str());
				}
				else if (position == 1) {
					newblock.largeur = atoi(value.c_str());
				}
				position++;
				value = "";
			}
			else {
				value = value + letter;
			}
		}
		newblock.profondeur = atoi(value.c_str());
		newblock.aire = newblock.largeur * newblock.profondeur;
		blockList.push_back(newblock);
	}
	for (int i = 0; i < argc; i++)
	{
		string argument = argv[i];
		if (argument == "-p") {
			afficherTour = true;
		}
		else if (argument == "-t") {
			afficherTemps = true;
		}
		else if (argument == "-r") {
			afficherHauteur = true;
		}
	}
	if (!blockList.empty())
	{
		clock_t begin = clock();

		sort(blockList.begin(), blockList.end(), comparerAire);
		glouton(blockList, afficherTour, afficherHauteur);

		clock_t end = clock();

		if (afficherTemps) {
			double elapsed_m_secs = (double(end - begin) / CLOCKS_PER_SEC) * 1000;
			cout << elapsed_m_secs << endl;
		}
	}
	else {
		cout << "Aucun bloc n'a été créé" << endl;
	}



}

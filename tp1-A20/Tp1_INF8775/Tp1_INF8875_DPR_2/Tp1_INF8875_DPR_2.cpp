// Tp1_INF8875_DPR_1.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
#include <float.h> 
#include <fstream>
#include <string>
#include <stdlib.h> 
#include <math.h> 
#include <list>
#include <ctime>
//#include "./stdc++.h"


// A divide and conquer program in C++  
// to find the smallest distance from a  
// given set of points.  

using namespace std;

// A structure to represent a Point in 2D plane  
class Point
{
public:
	int x, y;
};

/* Following two functions are needed for library function qsort().
Refer: http://www.cplusplus.com/reference/clibrary/cstdlib/qsort/ */

// Needed to sort array of points  
// according to X coordinate  
int compareX(const void* a, const void* b)
{
	Point* p1 = (Point*)a, * p2 = (Point*)b;
	return (p1->x - p2->x);
}

// Needed to sort array of points according to Y coordinate  
int compareY(const void* a, const void* b)
{
	Point* p1 = (Point*)a, * p2 = (Point*)b;
	return (p1->y - p2->y);
}

// A utility function to find the  
// distance between two points  
float dist(Point p1, Point p2)
{
	return sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
}

// A Brute Force method to return the  
// smallest distance between two points  
// in P[] of size n  
float bruteForce(Point P[], int n)
{
	float min = FLT_MAX;
	for (int i = 0; i < n; ++i) {
		for (int j = i + 1; j < n; ++j) {
			if (dist(P[i], P[j]) < min)
			{
				min = dist(P[i], P[j]);
			}
		}
	}
	return min;
}

// A utility function to find  
// minimum of two float values  
float min(float x, float y)
{
	return (x < y) ? x : y;
}


// A utility function to find the  
// distance beween the closest points of  
// strip of given size. All points in  
// strip[] are sorted accordint to  
// y coordinate. They all have an upper 
// bound on minimum distance as d.  
// Note that this method seems to be  
// a O(n^2) method, but it's a O(n)  
// method as the inner loop runs at most 6 times  
float stripClosest(Point strip[], int size, float d)
{
	float min = d; // Initialize the minimum distance as d  

	qsort(strip, size, sizeof(Point), compareY);

	// Pick all points one by one and try the next points till the difference  
	// between y coordinates is smaller than d.  
	// This is a proven fact that this loop runs at most 6 times  
	for (int i = 0; i < size; ++i)
		for (int j = i + 1; j < size && (strip[j].y - strip[i].y) < min; ++j)
			if (dist(strip[i], strip[j]) < min)
				min = dist(strip[i], strip[j]);

	return min;
}

// A recursive function to find the  
// smallest distance. The array P contains  
// all points sorted according to x coordinate  
float closestUtil(Point P[], int n)
{
	// If there are 2 or 3 points, then use brute force  
	if (n <= 12)
		return bruteForce(P, n);

	// Find the middle point  
	int mid = n / 2;
	Point midPoint = P[mid];

	// Consider the vertical line passing  
	// through the middle point calculate  
	// the smallest distance dl on left  
	// of middle point and dr on right side  
	float dl = closestUtil(P, mid);

	unique_ptr<Point[]> Pmid;
	Pmid.reset(new Point[n - mid]);
	for (int i = 0; i < (n - mid); i++) {
		Pmid[i] = P[mid + i];
	}

	float dr = closestUtil(Pmid.get(), n - mid);

	// Find the smaller of two distances  
	float d = min(dl, dr);

	// Build an array strip[] that contains  
	// points close (closer than d)  
	// to the line passing through the middle point  
	unique_ptr<Point[]> strip(new Point[n]);

	int j = 0;
	for (int i = 0; i < n; i++)
		if (abs(P[i].x - midPoint.x) < d)
			strip[j] = P[i], j++;

	// Find the closest points in strip.  
	// Return the minimum of d and closest  
	// distance is strip[]  
	return min(d, stripClosest(strip.get(), j, d));
}

// The main function that finds the smallest distance  
// This method mainly uses closestUtil()  
float closest(Point P[], int n)
{
	qsort(P, n, sizeof(Point), compareX);

	// Use recursive function closestUtil() 
	// to find the smallest distance 
	return closestUtil(P, n);
}

int main(int argc, char* argv[])
{
	// Create a text string, which is used to output the text file
	string myText;

	unique_ptr<Point[]> pointList;
	int listIndex = 0;
	int listSize = 0;

	// Read from the text file
	ifstream MyReadFile(argv[1]);

	bool firstLine = true;
	// Use a while loop together with the getline() function to read the file line by line

	while (getline(MyReadFile, myText)) {
		// The first line give the number of points to read
		if (firstLine) {
			listSize = atoi(myText.c_str());
			pointList.reset(new Point[listSize]);
			firstLine = false;
		}
		else {

			char letter = ';';
			string xPos = "";
			string yPos = "";
			int i = 0;

			// Define xPos of the point
			while ((letter = myText[i]) != ' ') {
				xPos += letter;
				i++;
			}

			// Jump the space character
			i++;

			// Define yPos of the point
			while (i < myText.length()) {
				letter = myText[i];
				yPos += letter;
				i++;
			}


			Point newPoint;
			newPoint.x = atoi(xPos.c_str());
			newPoint.y = atoi(yPos.c_str());

			pointList.get()[listIndex] = newPoint;
			listIndex++;
		}
	}

	// Start proces Algorithm
	clock_t begin = clock();

	if (listSize > 0) {
		float minDist = closest(pointList.get(), listSize);
		for (int i = 2; i < argc; i++) {
			string argument = argv[i];
			if (argument == "-p") {
				cout << minDist << endl;
				// apply the argument 1 time
				break;
			}
		}
	}
	else {
		cout << "Aucun point n'a été créé" << endl;
	}

	clock_t end = clock();

	double elapsed_m_secs = (double(end - begin) / CLOCKS_PER_SEC) * 1000;

	for (int i = 2; i < argc; i++) {
		string argument = argv[i];
		if (argument == "-t") {
			cout << elapsed_m_secs << endl;
			// apply the argument 1 time
			break;
		}
	}

	// Close the file
	MyReadFile.close();

	pointList = nullptr;

	return 0;
}
// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.

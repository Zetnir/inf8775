#!/bin/sh

if [ $1 = -a ]
then
	if [ $2 = vorace ]
	then
		if [ $3 = -e ]
		then
			if [ "$(expr substr $(uname -s) 1 5)" = "Linux" ];
			then
				./Glouton $4 $5 $6 $7
			else
				./Algo_Glouton.exe $4 $5 $6 $7
			fi	
		fi
	fi
	
	if [ $2 = progdyn ]
	then
		if [ $3 = -e ]
		then
			if [ "$(expr substr $(uname -s) 1 5)" = "Linux" ];
			then
				./ProgDyn $4 $5 $6 $7
			else
				./Algo_ProgDyn.exe $4 $5 $6 $7
			fi
		fi
	fi
	
	if [ $2 = tabou ]
	then
		if [ $3 = -e ]
		then
			if [ "$(expr substr $(uname -s) 1 5)" = "Linux" ];
			then
				./Tabou $4 $5 $6 $7
			else
				./Algo_Tabou.exe $4 $5 $6 $7
			fi
		fi
	fi
fi
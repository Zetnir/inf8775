// Tp1_INF8775.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
#include <float.h> 
#include <fstream>
#include <string>
#include <stdlib.h> 
#include <math.h> 
#include <list>
#include <ctime>

using namespace std;

// A structure to represent a Point in 2D plane 
struct Point
{
	int x, y;
};

// A utility function to find the distance between two points 
float dist(Point p1, Point p2)
{
	return sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
}

// A Brute Force method to return the smallest distance between two points 
// in P[] of size n 
float bruteForce(Point P[], int n)
{
	float min = FLT_MAX;
	for (int i = 0; i < n; ++i)
		for (int j = i + 1; j < n; ++j)
			if (dist(P[i], P[j]) < min) {
				min = dist(P[i], P[j]);
			}
	return min;
}

int main(int argc, char* argv[])
{
	// Create a text string, which is used to output the text file
	string myText;

	unique_ptr<Point[]> pointList;
	int listIndex = 0;
	int listSize = 0;

	// Read from the text file
	ifstream MyReadFile(argv[1]);

	bool firstLine = true;
	// Use a while loop together with the getline() function to read the file line by line

	while (getline(MyReadFile, myText)) {
		// The first line give the number of points to read
		if (firstLine) {
			listSize = atoi(myText.c_str());
			pointList.reset(new Point[listSize]);
			firstLine = false;
		}
		else {

			char letter = ';';
			string xPos = "";
			string yPos = "";
			int i = 0;

			// Define xPos of the point
			while ((letter = myText[i]) != ' ') {
				xPos += letter;
				i++;
			}

			// Jump the space character
			i++;

			// Define yPos of the point
			while ( i < myText.length()) {
				letter = myText[i];
				yPos += letter;
				i++;
			}


			Point newPoint;
			newPoint.x = atoi(xPos.c_str());
			newPoint.y = atoi(yPos.c_str());

			pointList.get()[listIndex] = newPoint;
			listIndex++;
		}
	}

	// Start proces Algorithm
	clock_t begin = clock();

	if (listSize > 0) {
		float minDist = bruteForce(pointList.get(), listSize);
		for (int i = 2; i < argc; i++) {
			string argument = argv[i];
			if (argument == "-p" ) {
				cout << minDist <<endl;
				// apply the argument 1 time
				break;
			}
		}
	}
	else {
		cout << "Aucun point n'a été créé" << endl;
	}

	clock_t end = clock();

	double elapsed_m_secs = (double(end - begin) / CLOCKS_PER_SEC) * 1000;

	for (int i = 2; i < argc; i++) {
		string argument = argv[i];
		if (argument == "-t") {
			cout << elapsed_m_secs << endl;
			// apply the argument 1 time
			break;
		}
	}

	// Close the file
	MyReadFile.close();

	pointList = nullptr;

	return 0;
}


// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.

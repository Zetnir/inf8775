Inspiré de https://stackoverflow.com/questions/2920416/configure-bin-shm-bad-interpreter
* Étape pour régler une erreur du type "bash: ./tp.sh : /bin/sh^M : mauvais interpré?teur: Aucun fichier ou dossier de ce type"
	1- vim ./tp.sh
	2- entrer dans le mode commande en appuyant sur la touche esc
	3- rentrer :set fileformat=unix
	4- enregistrer :wq!
	
* Donner la permission au tp.sh et aux exécutables
	1- chmod 777 ./tp.sh
	2- chmod 777 ./ProgDyn
	3- chmod 777 ./Glouton
	4- chmod 777 ./Tabou
	
* Exécuter de tp.sh
	tp.sh -a [vorace | progdyn | tabou] -e [path_vers_exemplaire]
	
	Arguments optionnels
		-p	affiche les blocs utilisés dans la construction de la tour chacun sur une ligne (hauteur, largeur, profondeur) en commençant par le bas.
		-t	affiche le temps d’exécution en ms, sans unité ni texte 
		-r affiche la hauteur maximum calculée de la tour
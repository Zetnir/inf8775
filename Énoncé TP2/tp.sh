#!/bin/sh

if [ $1 = -a ]
then
	if [ $2 = vorace ]
	then
		if [ $3 = -e ]
		then
			./TP2_INF8775/Debug/Algo_Glouton.exe $4 $5 $6 $7
		fi
	fi
	
	if [ $2 = progdyn ]
	then
		if [ $3 = -e ]
		then
			./TP2_INF8775/Debug/Algo_ProgDyn.exe $4 $5 $6 $7
		fi
	fi
	
	if [ $2 = tabou ]
	then
		if [ $3 = -e ]
		then
			./TP2_INF8775/Debug/Algo_Tabou.exe $4 $5 $6 $7
		fi
	fi
fi
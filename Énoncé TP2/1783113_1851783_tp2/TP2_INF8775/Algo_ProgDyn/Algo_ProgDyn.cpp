// Algo_Glouton.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
#include <algorithm>
#include <string>
#include <fstream>
#include <vector>
#include <ctime>


using namespace std;


struct Block {
	int hauteur = 0, largeur = 0, profondeur = 0, aire = 0;
};
struct TowerTemp {
	int maxHTemp = 0;
	vector<Block> tower;
};

bool comparerAire(const Block& a, const Block& b) {
	return a.aire > b.aire;
}

//inspiré de : https://www.tutorialspoint.com/Box-Stacking-Problem
void programmation_dynamique(vector<Block> blockList, bool afficherTour, bool afficherHauteur) {
	int numberOfBlock = blockList.size();
	vector<TowerTemp> maxHTemp;

	for (vector<Block>::iterator it = blockList.begin(); it != blockList.end(); it++) {
		TowerTemp towerTemp;
		towerTemp.maxHTemp = it->hauteur;
		towerTemp.tower.push_back(*it);
		maxHTemp.push_back(towerTemp);
	}

	for (int i = 1; i < numberOfBlock; i++) {
		for (int j = 0; j < i; j++) {
			if (blockList[i].largeur < blockList[j].largeur &&
				blockList[i].profondeur < blockList[j].profondeur &&
				maxHTemp[i].maxHTemp < maxHTemp[j].maxHTemp + blockList[i].hauteur) {
				maxHTemp[i].maxHTemp = maxHTemp[j].maxHTemp + blockList[i].hauteur;
				maxHTemp[i].tower = maxHTemp[j].tower;
				maxHTemp[i].tower.push_back(blockList[i]);
			}
		}
	}

	TowerTemp finalTower;
	finalTower.maxHTemp = -1;
	for (int i = 0; i < numberOfBlock; i++) {
		if (finalTower.maxHTemp < maxHTemp[i].maxHTemp) {
			finalTower.maxHTemp = maxHTemp[i].maxHTemp;
			finalTower.tower = maxHTemp[i].tower;
		}
	}
	if (afficherTour) {
		for (int i = 0; i < finalTower.tower.size(); i++) {
			cout << finalTower.tower[i].hauteur << " " << finalTower.tower[i].largeur << " " << finalTower.tower[i].profondeur << endl;
		}
	}
	if (afficherHauteur) {
		cout << finalTower.maxHTemp << endl;
	}
}


int main(int argc, char* argv[])
{
	vector<Block> blockList;
	int hauteurTour = 0;
	bool afficherTour = false;
	bool afficherTemps = false;
	bool afficherHauteur = false;

	//lecture fichier et creation des blocs
	string line;
	ifstream MyReadFile(argv[1]);

	while (getline(MyReadFile, line)) {
		Block newblock;
		string value = "";
		int position = 0;
		for (auto letter : line) {
			if (letter == ' ') {
				if (position == 0) {
					newblock.hauteur = atoi(value.c_str());
				}
				else if (position == 1) {
					newblock.largeur = atoi(value.c_str());
				}
				position++;
				value = "";
			}
			else {
				value = value + letter;
			}
		}
		newblock.profondeur = atoi(value.c_str());
		newblock.aire = newblock.largeur * newblock.profondeur;
		blockList.push_back(newblock);
	}
	for (int i = 0; i < argc; i++)
	{
		string argument = argv[i];
		if (argument == "-p") {
			afficherTour = true;
		}
		else if (argument == "-r") {
			afficherHauteur = true;
		}
		else if (argument == "-t") {
			afficherTemps = true;
		}
	}
	if (!blockList.empty())
	{
		clock_t begin = clock();

		sort(blockList.begin(), blockList.end(), comparerAire);
		programmation_dynamique(blockList, afficherTour, afficherHauteur);

		clock_t end = clock();

		if (afficherTemps) {
			double elapsed_m_secs = (double(end - begin) / CLOCKS_PER_SEC) * 1000;
			cout <<elapsed_m_secs << endl;
		}
	}
	else {
		cout << "Aucun bloc n'a été créé" << endl;
		//test
	}



}

// TP3.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
#include <string>
#include <fstream>
#include <sstream> 
#include <vector> 
#include <ctime>
#include <stdlib.h>


using namespace std;

struct Municipalite {
	int id = 0, value = 0, row = -1, col = -1;
	bool has_circon = false;
};

struct Circonscription {
	string circon_nom = "";
	vector<Municipalite> muni_in_circon;
	int circon_max_size = 0;
};

int random(int min, int max) {
	return (min + rand() % (max + 1 - min));
}

vector<Circonscription> initialiserCirconscription(int n, int m) {
	int num_of_circon = 0, count_of_k_min = 0, count_of_k_max = 0, num_of_k_max = n % m, k_min = n / m, k_max = k_min + 1;
	vector<Circonscription> all_circon_vect;
	while (num_of_circon != m) {

		Circonscription new_circon;
		new_circon.circon_nom = to_string(num_of_circon);

		if (count_of_k_max != num_of_k_max) {
			new_circon.circon_max_size = k_max;
			count_of_k_max++;
		} else {
			new_circon.circon_max_size = k_min;
		}

		num_of_circon++;
		all_circon_vect.push_back(new_circon);
	}

	return all_circon_vect;
}

int distanceManhanttan(Municipalite muni_1, Municipalite muni_2) {
	return (abs(muni_2.row - muni_1.row) + abs(muni_2.col - muni_1.col));
}

bool bonneDistanceManhattan(int dist_max, Municipalite muni_1, Municipalite muni_2) {
	return (distanceManhanttan(muni_1, muni_2) <= dist_max);
}

vector<Municipalite> correctMuniPossible(vector<vector<Municipalite>>& all_muni_vec, vector<Municipalite> muni_possible, int circon_max_size) {
	vector<Municipalite> muni_possible_temp = muni_possible;
	while (muni_possible_temp.size() > circon_max_size) {
		int max_dist = 0;
		vector<int> max_position2;
		for (int i = 0; i < muni_possible_temp.size() - 1; i++) {
			for (int j = i; j < muni_possible_temp.size(); j++) {
				int distance = distanceManhanttan(muni_possible_temp[i], muni_possible_temp[j]);
				if (distance == max_dist) {
					max_dist = distance;
					max_position2.push_back(j);
				} else if (distance > max_dist) {
					max_dist = distance;
					max_position2.clear();
					max_position2.push_back(j);
				}
			}
		}
		int position = random(0, max_position2.size() - 1);
		vector<Municipalite>::iterator it = muni_possible_temp.begin() + max_position2[position];
		all_muni_vec[it->row][it->col].has_circon = false;
		muni_possible_temp.erase(it);
	}
	return muni_possible_temp;
}

vector<Municipalite> remplirUneCirconscription(vector<vector<Municipalite>>& all_muni_vec, int dist_max, int circon_max_size) {
	vector<Municipalite> muni_possible;
	bool found_muni = false;
	int i_libre = 0, j_libre = 0;
	for (i_libre = 0; i_libre < all_muni_vec.size(); i_libre++) {
		for (j_libre = 0; j_libre < all_muni_vec[i_libre].size(); j_libre++) {
			if (!all_muni_vec[i_libre][j_libre].has_circon) {
				found_muni = true;
				break;
			}
		}
		if (found_muni) {
			break;
		}
	}

	for (int i = i_libre; (i < all_muni_vec.size()) && (i <= i_libre + dist_max); i++) {
		int position_j = (i == i_libre) ? (j_libre) : 0;
		for (int j = position_j; (j < all_muni_vec[i].size()) && (j < j_libre + dist_max); j++) {
			if (bonneDistanceManhattan(dist_max, all_muni_vec[i_libre][j_libre], all_muni_vec[i][j]) && !all_muni_vec[i][j].has_circon) {
				muni_possible.push_back(all_muni_vec[i][j]);
				all_muni_vec[i][j].has_circon = true;
			}
		}
	}

	return correctMuniPossible(all_muni_vec, muni_possible, circon_max_size);
}

bool remplirCirconscriptions(vector<vector<Municipalite>>& all_muni_vec, vector<Circonscription>& all_circon_vec, int dist_max) {
	for (int i = 0; i < all_circon_vec.size(); i++) {
		all_circon_vec[i].muni_in_circon = remplirUneCirconscription(all_muni_vec, dist_max, all_circon_vec[i].circon_max_size);
		if (all_circon_vec[i].muni_in_circon.size() != all_circon_vec[i].circon_max_size) {
			return true;
		}
	}
	return false;
}

void print(vector<Circonscription> all_circon_vec) {
	for (int i_circon = 0; i_circon < all_circon_vec.size(); i_circon++) {
		for (int j = 0; j < all_circon_vec[i_circon].muni_in_circon.size(); j++) {
			cout << all_circon_vec[i_circon].muni_in_circon[j].col << " " << all_circon_vec[i_circon].muni_in_circon[j].row << " ";
		}
		cout << endl;
	}
}

int getWinner(vector<Circonscription> all_circon_vec) {
	int counter_circon_vert_gagnant = 0;
	for (int i = 0; i < all_circon_vec.size(); i++) {
		int number_max = all_circon_vec[i].circon_max_size * 100;
		int counter_vote = 0;
		for (int j = 0; j < all_circon_vec[i].muni_in_circon.size(); j++) {
			counter_vote += all_circon_vec[i].muni_in_circon[j].value;
		}
		if (counter_vote > (number_max / 2)) {
			counter_circon_vert_gagnant++;
		}
	}
	return counter_circon_vert_gagnant;
}

int main(int argc, char* argv[]) {
	string line;
	ifstream MyReadFile(argv[1]);
	int m = strtol(argv[3], NULL, 10); // modifie donne a lire
	int rows = 0, cols = 0;
	vector<vector<Municipalite>> all_muni_vec;
	vector<Circonscription> all_circon_vec;

	int dist_max = 0, n = 0;

	// lecture nombre de row et col
	getline(MyReadFile, line);
	istringstream first_row(line);
	first_row >> rows >> cols;
	n = rows * cols;

	dist_max = (n % (2 * m) == 0) ? (n / (2 * m)) : ((n / (2 * m)) + 1);

	// lecture donnee
	int id_counter = 0;
	int row_counter = 0;
	while (getline(MyReadFile, line)) {
		istringstream line_stream(line);
		int num;
		vector<Municipalite> row_temp;

		int col_counter = 0;
		while (line_stream >> num) {
			Municipalite temp_muni;
			temp_muni.id = id_counter;
			temp_muni.value = num;
			temp_muni.row = row_counter;
			temp_muni.col = col_counter;
			col_counter++;
			id_counter++;
			row_temp.push_back(temp_muni);
		}
		all_muni_vec.push_back(row_temp);
		row_counter++;
	}

	while (true) {
		bool repeter = true;
		while (repeter) {
			bool afficher_sol = false;
			all_circon_vec = initialiserCirconscription(n, m);
			repeter = remplirCirconscriptions(all_muni_vec, all_circon_vec, dist_max);
			if (!repeter) {
				for (int i = 0; i < argc; i++) {
					string argument = argv[i];
					if (argument == "-p") {
						afficher_sol = true;
					}
				}
				if (!afficher_sol) {
					int num_win = getWinner(all_circon_vec);
					cout << num_win << endl;

				} else {
					print(all_circon_vec);
				}
			}
			all_circon_vec.clear();
		}
	}


}
